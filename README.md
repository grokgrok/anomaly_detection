
I believe the optimal solution entails use of:

1. The Welford algorithm for generating mean and variance from a stream.
2. The Welford algorithm for adding and removing single values from the implicit population of a given mean and variance.
3. The use of composite means and variances for generating the mean and variance of a population with only the mean and variance of samples. 

However, I was unable to get this to work after getting a brute-force solution to work, which I am submitting.

Using the preceding statistics, I maintained the mean/variance for every user for their network at depth 1. So, for example, to generate the mean/variance for a network of depth 2, I created the composite mean/variance from my network of depth 1.

I was unable to complete this implementation, however, and had trouble dealing with duplicates among samples for a network of depth N.

As a result, I merely implemented a highly concurrent though non-scalable solution. 

I appreciate the opportunity. I regret the timing and missteps. 

My code is not polished nor documented because I spent my time on the preceding. I did not check in my test utilities.

-- Brian