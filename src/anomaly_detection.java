//package insightCodingChallenge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.logging.*;



public class anomaly_detection {

	static anomaly_detection This = new anomaly_detection();
	static DecimalFormat df = new DecimalFormat("#.00");
	
	static Logger LOGGER = Logger.getLogger(This.getClass().getName());
	static boolean LOG_STREAM_PARSE = false; // Show each json record and parsing result
	static boolean LOG_PURCHASE_TRANSACTIONS = false;  // Show additions and removals of purchase history as it happens
	static boolean LOG_NETWORKING = false;  // Show generation of user network to maxDepth
	static boolean LOG_SOCIAL_GRAPH = false;  // Show adj matrix 
	static boolean LOG_PURCHASE_HISTORY = false; // Show current mapping of userid to purchase history
	
	static boolean LOG_STATS_ARRAYS = false; // Generate arrays of purchases used to produce stats and put into Welford
	static boolean LOG_STATS_PURCHASE_HISTORY = false; // Show history of purchases for stats as it builds the debugging arrays
	static boolean LOG_OUTLIERS = false; // log generated records

	static boolean LOG_RUN = true;
	
	class Purchase  {
		public int m_id; // tbo redundant 
		public long m_amt; 
		public BigDecimal m_tsx;  // Not optimal, but in a rush. Idea is a bivariate key. Given no constraints provided, meaning billions of identical timestamps are possible, this works 
		public long m_ts;  // tbo redundant wrt m_tsx
		public int m_tsCollision; // ditto

		public Purchase(int id, long amt, long ts, int tsCollision) {
			m_id = id;  // Valid id checked?
			m_amt = amt; // Assume US currency, resolution 1 cent
			m_ts = ts;
			m_tsCollision = tsCollision;
			m_tsx = new BigDecimal(m_ts + "." + m_tsCollision);
			// https://stackoverflow.com/questions/3752578/safe-string-to-bigdecimal-conversion
		}

		@Override
		public String toString() {
			return "\n" + m_id + "\t" + m_amt + "\t" + m_ts + "\t" + m_tsCollision + "\t" + m_tsx;
		}
				
		@Override
		public int hashCode() {
			return Objects.hash(m_id,m_amt,m_ts,m_tsCollision);
		}
		
		@Override
		public boolean equals(Object o) {
			if (o == this) {
				return true;
			}
			if ( ! (o instanceof Purchase) ) {
				return false;
			}
			Purchase p = (Purchase) o;
			if (this.m_ts == p.m_ts && this.m_id == p.m_id && this.m_amt == p.m_amt && this.m_tsCollision == p.m_tsCollision) {
				return true;
			} else {
				return false;
			}			
		}
	}
	
	class purchaseHistory {
		int m_max;
		int m_id;
		private ConcurrentSkipListMap<BigDecimal,Purchase> m_basket;
		
		public purchaseHistory(int id, int max) {
			m_max = max;
			m_id = id;
			m_basket = new ConcurrentSkipListMap<BigDecimal,Purchase>(Collections.reverseOrder());
		}
		
		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append(m_id);
			sb.append("\t");
			sb.append(m_max);
			sb.append("\t");
			Iterator<Purchase> it = m_basket.values().iterator();
			while(it.hasNext()) {
				sb.append(it.next());
				//sb.append("\n");
			}
			return sb.toString();
		}
		
		public void add(Purchase p) {
			if (m_basket.size() == m_max) {
				if (LOG_PURCHASE_TRANSACTIONS) {
					StringBuffer sb = new StringBuffer();
					sb.append("\nBEFORE\n" + this);
					sb.append("Eject and Replace \n\t" + m_basket.lastEntry() + " \nwith\n\t " + p);
					LOGGER.info(sb.toString());
				}
				m_basket.remove(m_basket.lastKey());
				m_basket.put(p.m_tsx, p);
				if (LOG_PURCHASE_TRANSACTIONS) {
					LOGGER.info("\nAFTER\n" + this);
				}
			}
			else {
				if (LOG_PURCHASE_TRANSACTIONS) {
					LOGGER.info("\nBEFORE\n" + this);
				}
					m_basket.put(p.m_tsx, p);
					if (LOG_PURCHASE_TRANSACTIONS) {
						LOGGER.info("\nAFTER\n" + this);
					}
			}
		}
	}

	
	class purchaseHistoryBroker {
		int m_max;
		int m_depth;
		socialNetwork m_network;
		long currentTS = 0;
		int currentTSCount = 0;
		ConcurrentHashMap<Integer,purchaseHistory> m_purchases;

		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append(m_max);
			sb.append("\t");
			sb.append(m_depth);
			sb.append("\n");
			Iterator<purchaseHistory> it = m_purchases.values().iterator();
			while(it.hasNext()) {
				sb.append(it.next());
				sb.append("\n");
			}
			return sb.toString();
		}
		
		public purchaseHistoryBroker(int max, int depth,socialNetwork network) {
			m_max = max;
			m_depth = depth;
			m_network = network;
			m_purchases = new ConcurrentHashMap<Integer,purchaseHistory>();
		}
		
		public void add(int id, long ts, long amt) {
			if (ts > currentTS) {
				currentTS = ts;
				currentTSCount = 0;
			}
			else {
				currentTSCount++;
			}
			Purchase p = This.new Purchase(id,amt,ts,currentTSCount);
			if (m_purchases.containsKey(id)) {
				m_purchases.get(id).add(p);
			} 
			else {
				purchaseHistory ph = This.new purchaseHistory(id,m_max);
				ph.add(p);
				m_purchases.put(id,ph);
			}
		}

		private ConcurrentSkipListMap<BigDecimal,purchaseHistory> getPurchases(HashSet<Integer> net) {
			ConcurrentSkipListMap<BigDecimal,purchaseHistory> kway = new ConcurrentSkipListMap<BigDecimal,purchaseHistory>(Collections.reverseOrder());
			for(int pid: net) {
				purchaseHistory ph = m_purchases.get(pid);
				if (ph != null) {
					kway.put(ph.m_basket.firstKey(), ph);
				}
			}
			return kway;
		}

	
		public HashSet<Integer> getNetwork(int id, int maxDepth,ConcurrentSkipListMap<BigDecimal,purchaseHistory> kway) {
				
			StringBuffer sb = null;
			if (LOG_NETWORKING) {
				sb = new StringBuffer();
			}
			HashSet<Integer> network = new HashSet<Integer>();
			LinkedHashMap<Integer,Integer> queue = new LinkedHashMap<Integer,Integer>(); // Apache would be better, but I am avoiding use of extra jars
			queue.put(id,0);
			while(queue.isEmpty()==false) {	
				Iterator<Integer> it = queue.keySet().iterator();
				int qid = it.next();
				int qlevel = queue.get(qid);
				it.remove();
				if (qid != id) {
					if (LOG_NETWORKING) {
						sb.append("UPDATING: " + id + "\t" + qid + "\t" + qlevel + "\t" + maxDepth+"\n");
					}
					network.add(qid);
					purchaseHistory ph = m_purchases.get(qid);
					if (ph != null) {
						kway.put(ph.m_basket.firstKey(), ph);
					}
				}
				if (qlevel < maxDepth) {
					socialNetworkFriends friends = m_network.m_network.get(qid);
					if (friends != null) {
						for(int fid: friends.m_friends) {
							if (network.contains(fid)==false && queue.containsKey(fid) == false) {
								queue.put(fid,qlevel+1);
							}
						}
					}
				}
			}
			if (LOG_NETWORKING) {
				LOGGER.info("\n"+sb.toString());
				LOGGER.info("\n"+id+": " + network);
			}
			return network;
		}
		
		public void getStats(int id,Welford w) {
			ConcurrentSkipListMap<BigDecimal,purchaseHistory> kway = new ConcurrentSkipListMap<BigDecimal,purchaseHistory>(Collections.reverseOrder());
			HashSet<Integer> net = getNetwork(id, this.m_depth, kway);
//			ConcurrentSkipListMap<BigDecimal,purchaseHistory> kway = getPurchases(net);


			for(int i = 0; i < this.m_max; i++) {
				if (kway.isEmpty() == true) {
					break; // out of purchases
				}
				BigDecimal key = kway.firstKey();
				purchaseHistory ph = kway.get(key);
				long amt = ph.m_basket.get(key).m_amt;
				w.handle((double) amt);
				kway.remove(key);
				BigDecimal newkey = ph.m_basket.higherKey(key);
				if (newkey != null) {
					kway.put(newkey,ph); // this user history is exhausted
				}
			}
		}
		
	}

	
	class socialNetworkFriends {
		int m_id;
		Set<Integer> m_friends;
		
		@Override
		public String toString() {
			return m_id + "\t" + m_friends;
		}
		
		public socialNetworkFriends(int id){
			m_id = id;
			m_friends = Collections.newSetFromMap(new ConcurrentHashMap<Integer,Boolean>());
		}
		public void befriend(int me, int id) {
			m_friends.add(id);
		}
		public void unfriend(int me, int id) {
			m_friends.remove(id);
		}
	}

	
	class socialNetwork {
		
		ConcurrentHashMap<Integer,socialNetworkFriends> m_network;

		public socialNetwork() {
			m_network = new ConcurrentHashMap<Integer,socialNetworkFriends>();
		}
		
		@Override 
		public String toString() {
			StringBuffer sb = new StringBuffer();
			for(int id: m_network.keySet()) {
				sb.append(m_network.get(id));
				sb.append("\n");
			}
			return sb.toString();
		}

		public void befriend(int id1, int id2) {
			if (m_network.containsKey(id1) == false) {
				socialNetworkFriends net = This.new socialNetworkFriends(id1);
				m_network.put(id1, net);
			}
			if (m_network.containsKey(id2) == false) {
				socialNetworkFriends net = This.new socialNetworkFriends(id2);
				m_network.put(id2, net);				
			}
			m_network.get(id1).befriend(id1,id2);
			m_network.get(id2).befriend(id2,id1); // tbo - this duplication can be optimized out with a better design
		}
		
		public void unfriend(int id1, int id2) {
			if (m_network.containsKey(id1) == true) {
				m_network.get(id1).unfriend(id1,id2);
			}
			if (m_network.containsKey(id2) == true) {
				m_network.get(id2).unfriend(id2,id1);  // tbo - this duplication can be optimized out with a better design
			}
		}
	
	}

	
	public static void process(json J, socialNetwork network, purchaseHistoryBroker broker, PrintWriter out) {
		if (J.verify()) {
			if (J.eventType == 'p') {
				Welford w = This.new Welford(J.amt);
				broker.getStats(J.id,w);
				
				if (w.anomalous()) {
					J.addField("mean",w.scaleMeanf());
					J.addField("sd",w.scaleSDf());
					out.println(J.newRec);
					if (This.LOG_OUTLIERS) {
						LOGGER.info(J.newRec);
					}
				}
								
				broker.add(J.id, J.ts, J.amt);
			}
			else if (J.eventType == 'b') {
				network.befriend(J.id1, J.id2);
			}
			else if (J.eventType == 'u') {
				network.unfriend(J.id1, J.id2);
			}
		}
	}

	
	
	public static void main(String[] args) throws IOException {		

		long tsb = System.currentTimeMillis();

		
		String initLog = null;
		String streamLog = null;
		String outLog = null;		
		if (args.length > 0) {
			initLog = args[0];
			streamLog = args[1];
			outLog = args[2];
		}
		else {
			initLog = "batch_log.json";
			streamLog = "stream_log.json";
			outLog = "out.json";			
		}
		PrintWriter out = new PrintWriter(outLog,"UTF-8");	
		
		socialNetwork network = This.new socialNetwork();		
		purchaseHistoryBroker broker = null;		
		json j = This.new json();
			

		BufferedReader bf = new BufferedReader(new FileReader(initLog));
		String config = bf.readLine();
		j.parseConfig(config);
		if (j.verifyConfig() == false) {
			LOGGER.warning("Failed to parse Configuration");
			return;
		}
		broker = This.new purchaseHistoryBroker(j.T,j.D,network);
	
		int cnt = 0;
		String line = null;
		while( (line = bf.readLine()) != null) {
			cnt++;
			if (LOG_RUN) {
				if ((cnt % 10000) == 0) {
					LOGGER.info(""+cnt);
				}
			}
			j.parseLine(line);
			if (LOG_STREAM_PARSE) {
				LOGGER.info(line + "\n" + j.verify() + "\t" + j);
			}
			if (j.verify() == false) {
				LOGGER.warning("Failure " + line);
			}
			else {
				process(j,network,broker,out);
			}
		}

		if (LOG_SOCIAL_GRAPH) {
			LOGGER.info("\n"+network);
		}
		if (LOG_PURCHASE_HISTORY) {
			LOGGER.info("\n"+broker);
		}
		
		bf = new BufferedReader(new FileReader(streamLog));
		while ( (line = bf.readLine()) != null) {
			j.parseLine(line);
			if (LOG_STREAM_PARSE) {
				LOGGER.info(line + "\n" + j.verify() + "\t" + j);
			}
			if (j.verify() == false) {
				LOGGER.warning("Failure " + line);
			}
			else {
				process(j,network,broker,out);
			}			
		}
		out.close();
		
		if (LOG_SOCIAL_GRAPH) {
			LOGGER.info("\n"+network);
		}
		if (LOG_PURCHASE_HISTORY) {
			LOGGER.info("\n"+broker);
		}
		
		long tse = System.currentTimeMillis();
		if (LOG_RUN) {
			LOGGER.info(""+(tse-tsb));
		}
		
	}

	
	
	
	
	
	
	
	
	
	// https://lingpipe-blog.com/2009/07/07/welford-s-algorithm-delete-online-mean-variance-deviation/
	// Normalizing to 0-1 allows for the *potential* to drop and add values
		class Welford {
			private long mN = 0L;
			private double mM = 0.0;
			private double mS = 0.0;

			private double m_val = 0.0;
			private double m_var = 0.0;
			private double m_stddev = 0.0;
			
			public Welford(double value) {
				m_val = value;
				mN = 0L;
				mM = 0.0;
				mS = 0.0;
				m_var = 0.0;
				m_stddev = 0.0;
			}
			
			public boolean anomalous() {
				if (mN <= 2) {
					return false;
				}
				stdDevUnbiased();
				double diff = 3.0 * m_stddev;
				double ub = mM + diff;
				return m_val > ub;
			}
			
			public String scaleMeanf() {
				mean();
				return df.format(mM/100);
			}
			
			public String scaleSDf() {
				varianceUnbiased();
				double v = this.m_var;
				if (v > 0) {
					v = v/10000;
					double stddev = Math.sqrt(v);
					return df.format(stddev);
				}
				else {
						return df.format(0.0);
				}
			}

			
			public void handle(double x) {
			     ++mN;
			    double nextM = mM + (x - mM) / mN;
			    mS += (x - mM) * (x - nextM);
			    mM = nextM;
			}
			
			// valid iff data are normalized to 0..1 afaik
			public void unHandle(double x) {
			    if (mN == 0) {
			        throw new IllegalStateException();
			    } else if (mN == 1) { 
			        mN = 0; mM = 0.0; mS = 0.0;
			    } else {
			        double mMOld = (mN * mM - x)/(mN);
			        mS -= (x -  mM) * (x - mMOld);
			        mM = mMOld;
			        --mN;
			    }
			}

			public double mean() {
			    return mM;
			}
			public double varianceUnbiased() {
			    m_var = mN > 1 ? mS/(mN) : 0.0;
			    return m_var;
			}
			public double stdDevUnbiased() {
			    m_stddev =  mN > 1 ? Math.sqrt(mS/(mN)) : 0.0;
			    return m_stddev;
			}
		}

		

// informal hack
class json {
	public Integer T;
	public Integer D;

	public String newRec;
	public String origRec;
	public String event;
	public Character eventType;
	public Long ts;
	public Integer id;
	public Long amt;
	public Integer id2;
	public Integer id1;
	public String error;
	
	public String addField(String label, String val) {
		if (newRec == null) {
			newRec = origRec;
		}
		int ix = newRec.indexOf("}");
		newRec = newRec.substring(0,ix) + ",\"" + label + "\"" + ":" + "\"" + val + "\"" + "}";
		return newRec;
	}
	
	@Override
	public String toString() {
		return "T: " + T + "\t D: " + D + "\t event: " + event + "\t eventType: " + eventType + "\t ts: " + ts + "\t id: " + id + "\t amt: " + amt + "\t id2: " + id2 + "\t id1: " + id1 + "\t ERROR: " + error;
	}
	
	public json() {
		origRec = null;
		newRec = null;
		T = null;
		D = null;
		event = null;
		eventType = null;
		ts = null;
		id = null;
		amt = null;
		id2 = null;
		id1 = null;
		error = null;
	}

	public boolean verifyConfig() {
		if (T == null) {
			return false;
		}
		if (D == null) {
			return false;
		}
		return true;
	}
	
	public boolean verify() {
		if (T == null) {
			LOGGER.warning("JSON reject: T is null");
			return false;
		}
		if (D == null) {
			LOGGER.warning("JSON reject: D is null");
			return false;
		}
		if (ts == null) {
			LOGGER.warning("JSON reject: ts is null");
			return false;
		}
		if (event == null) {
			LOGGER.warning("JSON reject: event is null");
			return false;
		}
		if (!(event.equals("purchase") || event.equals("befriend") || event.equals("unfriend"))) {
			LOGGER.warning("JSON reject: event is invalid is null");
			return false;
		}
		if (event.equals("purchase")) {
			if (amt == null) {
				LOGGER.warning("JSON reject: amt is null");
				return false;
			}
			if (id == null) {
				LOGGER.warning("JSON reject: id is null");
				return false;
			}
			if (amt < 0) {
				LOGGER.warning("JSON reject: negative purchase amount. Refunds ignored.");
				return false;
			}
		}
		else {
			if (id1 == null) {
				LOGGER.warning("JSON reject: id1 is null");
				return false;
			}
			if (id2 == null) {
				LOGGER.warning("JSON reject: id2 is null");
				return false;
			}
			if (id1 == id2) {
				LOGGER.warning("JSON reject: friending or unfriending self");
				return false;
			}
		}
		return true;
	}
	
	protected void parseConfig(String s) {
		try {
				newRec = null;
				origRec = s;
				D = null;
				T = null;
				error = null;
				s = s.toLowerCase().replace("\"","").replace("{","").replace("}","").trim(); // tbf use regex
				String[] rec = s.split(",");
				rec[0] = rec[0].trim();
				rec[1] = rec[1].trim();
				if (rec[0].charAt(0) == 't') {
					T=Integer.parseInt(rec[0].substring(rec[0].indexOf(':')+1));
					if (rec[1].charAt(0) == 'd') {
						D=Integer.parseInt(rec[1].substring(rec[0].indexOf(':')+1));
					}
				}			
				if (rec[0].charAt(0) == 'd') {
					D=Integer.parseInt(rec[0].substring(rec[0].indexOf(':')+1));
					if (rec[1].charAt(0) == 't') {
						T=Integer.parseInt(rec[1].substring(rec[0].indexOf(':')+1));
					}
				}			
			}
		catch (Exception e) {
			error = e.toString();
			e.printStackTrace(); // proxy for logging log errors
		}
	}
		
	protected void parseLine(String s) {
		try {
				newRec = null;
				origRec = s;
				event = null;
				id = null;
				amt = null;
				ts = null;
				id2 = null;
				s = s.toLowerCase().replace("\"","").replace("{","").replace("}","").trim(); // tbf use regex
				String[] rec = s.split(",");
				for(String pair: rec) {
					int colonSplit = pair.indexOf(":");
					String key = pair.substring(0,colonSplit).trim();
					String val = pair.substring(colonSplit+1).trim();
					if (key.equals("event_type")) {
						event = val;
						if (val.length() > 0) {
							eventType = val.charAt(0);
						}
					} 
					else if (key.equals("timestamp")) {
						val = val.replace("-","");
						val = val.replace(":","");
						val = val.replace(" ","");
						if (val.length() == 14) {
							ts = Long.parseLong(val);
						}
					}
					else if (key.equals("id")) {
						id = Integer.parseInt(val);
					}
					else if (key.equals("id1")) {
						id1 = Integer.parseInt(val);					
					}
					else if (key.equals("id2")) {
						id2 = Integer.parseInt(val);
					}
					else if (key.equals("amount")) {
						Double d = Double.parseDouble(val);
						amt = (long) (d * 100.0);
					}
				}	
		}
		catch(Exception e) {
			error = e.toString();
			LOGGER.warning(e.toString());
		}
	}
	
}

}


